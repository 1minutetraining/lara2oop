<?php


namespace Application\Middlewares;


use Application\Exceptions\AuthException;
use Application\Middlewares\Contracts\Middleware;

class AuthMiddleware extends Middleware
{

    protected function handle()
    {
//        $_SESSION['loggedIn'] = userID;
//        if(isset($_SESSION['loggedIn']) && intval($_SESSION['loggedIn']) > 0)
//        {
//            return true;
//        }else{
//            return false;
//        }
        if(!$this->isUserLoggedIn())
        {
            throw new AuthException('user not logged in!');
        }
    }

    private function isUserLoggedIn()
    {
        isset($_SESSION['loggedIn']) && intval($_SESSION['loggedIn']) > 0;
    }
}