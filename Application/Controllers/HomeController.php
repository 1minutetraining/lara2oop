<?php


namespace Application\Controllers;


use Carbon\Carbon;

class HomeController
{
    public function index()
    {
        echo Carbon::now('Asia/Tehran');
    }
}